package ga.jakeobrien.shalombot3.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;

public class ConfigManager {
    public static JsonObject config;
    public static void init() throws FileNotFoundException {
        File configFile = getFileFromResources("config.json");
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(configFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        config = gson.fromJson(br, JsonObject.class);
    }

    private static File getFileFromResources(String fileName) throws FileNotFoundException {
        URL resource = ConfigManager.class.getClassLoader().getResource(fileName);
        if (resource == null) {
            throw new FileNotFoundException();
        } else {
            return new File(resource.getFile());
        }

    }
}
