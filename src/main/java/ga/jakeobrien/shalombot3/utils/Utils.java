package ga.jakeobrien.shalombot3.utils;

import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.object.entity.*;
import discord4j.core.object.reaction.ReactionEmoji;
import discord4j.core.object.util.Snowflake;
import discord4j.rest.http.client.ClientException;
import ga.jakeobrien.shalombot3.Main;

import java.util.Objects;

public class Utils {
    public static void incrementName(Member m) {
        int i;
        String nick =  m.getNickname().orElse(m.getDisplayName());

        for (i = nick.length() - 1; i > 0; i--) {
            if (!Character.isDigit(nick.charAt(i))) {
                i++;
                break;
            }
        }

        int index = i;
        if (i != nick.length()) {
            StringBuilder sb = new StringBuilder();
            for (; i < nick.length(); i++) {
                sb.append(nick.charAt(i));
            }
            int number = Integer.parseInt(sb.toString());
            String newnick = nick.substring(0, index);
            newnick = newnick + (number + 1);
            final String finalNick = newnick;
            try {
                m.edit(spec -> spec.setNickname(finalNick)).block();
            } catch (ClientException e) {
                System.out.println("Caught an exception while attempting to increment the name of " + nick + ". Are there sufficient permissions?");
            }
        }

    }
    public static int nextTicketNumber() {
        for (int i = 1; i < Integer.MAX_VALUE; i++) {
            if (!containsChannel("ticket-" + i)) {
                return i;
            }
        }
        return -1;
    }

    private static boolean containsChannel(String name) {
        for (GuildChannel guildChannel : Objects.requireNonNull(Main.client.getGuildById(Snowflake.of(ConfigManager.config.get("guild").getAsJsonObject().get("id").getAsLong())).block().getChannels().collectList().block())) {
            if (guildChannel.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    public static void sendCloser(MessageChannel channel) {
        Message msg = channel.createMessage("Thank you for your ticket with Shalom Support. Please react with an ❌ to indicate you are ready to close this ticket.").block();
        msg.addReaction(ReactionEmoji.unicode("❌")).block();
    }

    public static void handleClose(ReactionAddEvent event) {
        if (event.getChannel().block().getType() == Channel.Type.GUILD_TEXT &&
                ((TextChannel) event.getChannel().block()).getName().startsWith("ticket-") &&
                !event.getUserId().equals(Main.client.getApplicationInfo().block().getId()) &&
                event.getMessage().block().getAuthorAsMember().block().getId().equals(Main.client.getSelf().block().getId()) &&
                event.getMessage().block().getContent().orElse("").equalsIgnoreCase("Thank you for your ticket with Shalom Support. Please react with an ❌ to indicate you are ready to close this ticket.")) {
            String url = Hastebin.postChannel((TextChannel) event.getChannel().block());
            event.getChannel().block().delete().block();
            ((TextChannel) Main.client.getChannelById(Snowflake.of(ConfigManager.config.get("guild").getAsJsonObject().get("ticket-logs").getAsLong())).block()).createMessage("Ticket logged at: " + url).block();
        }
    }

}
