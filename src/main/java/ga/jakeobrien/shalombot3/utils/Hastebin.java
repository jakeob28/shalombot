package ga.jakeobrien.shalombot3.utils;

import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.TextChannel;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class Hastebin {

	public static String postChannel(TextChannel channel) {
		StringBuilder sb = new StringBuilder();

		for (Message message : Objects.requireNonNull(channel.getMessagesBefore(channel.getLastMessageId().get()).collectList().block())) {
			if (message.getContent().isPresent())
				sb.insert(0, message.getAuthorAsMember().block().getNickname().orElse(message.getAuthorAsMember().block().getDisplayName()) +
						" (" + message.getTimestamp().toString() + "): " + message.getContent().get() + "\n");
		}

		try {
			return post(sb.toString(), false);
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String post(String text, boolean raw) throws IOException {
		byte[] postData = text.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;

		String requestURL = "https://hasteb.in/documents";
		URL url = new URL(requestURL);
		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("User-Agent", "Hastebin Java Api");
		conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
		conn.setUseCaches(false);

		String response = null;
		DataOutputStream wr;
		try {
			wr = new DataOutputStream(conn.getOutputStream());
			wr.write(postData);
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			response = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (response.contains("\"key\"")) {
			response = response.substring(response.indexOf(":") + 2, response.length() - 2);

			String postURL = raw ? "https://hasteb.in/raw/" : "https://hasteb.in/";
			response = postURL + response;
		}

		return response;
	}

}