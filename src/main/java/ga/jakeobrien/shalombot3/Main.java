package ga.jakeobrien.shalombot3;

import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.domain.guild.MemberJoinEvent;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.object.entity.Channel;
import discord4j.core.object.entity.User;
import discord4j.core.object.util.Snowflake;
import ga.jakeobrien.shalombot3.commands.AddCommand;
import ga.jakeobrien.shalombot3.commands.CloseCommand;
import ga.jakeobrien.shalombot3.commands.CommandManager;
import ga.jakeobrien.shalombot3.commands.TicketCommand;
import ga.jakeobrien.shalombot3.responses.*;
import ga.jakeobrien.shalombot3.utils.ConfigManager;
import ga.jakeobrien.shalombot3.utils.Utils;

import java.io.FileNotFoundException;

public class Main {
    public static DiscordClient client;
    public static void main(String[] args) throws FileNotFoundException {
        ConfigManager.init();

        CommandManager.register(new TicketCommand());
        CommandManager.register(new CloseCommand());
        CommandManager.register(new AddCommand());

        ResponseManager.register(new RealClijmartResponse());
        ResponseManager.register(new FellowMinecrafterResponse());
        ResponseManager.register(new SoSadResponse());
        ResponseManager.register(new BigOofResponse());
        ResponseManager.register(new PurimResponse());
        ResponseManager.register(new GGBasResponse());
        ResponseManager.register(new NiceClijResponse());
        ResponseManager.register(new LazyBoiResponse());
        ResponseManager.register(new WaterPumpResponse());
        ResponseManager.register(new DooDooDahResponse());
        ResponseManager.register(new ShouldWeKillfarmResponse());

        DiscordClientBuilder builder = new DiscordClientBuilder(ConfigManager.config.get("token").getAsString());
        client = builder.build();
        client.getEventDispatcher().on(ReadyEvent.class)
                .subscribe(event -> {
                    User self = event.getSelf();
                    System.out.println(String.format("Logged in as %s#%s", self.getUsername(), self.getDiscriminator()));
                });

        client.getEventDispatcher().on(MessageCreateEvent.class)
                .subscribe(event -> {
                    CommandManager.onMessage(event);
                    ResponseManager.onMessage(event);
                });

        client.getEventDispatcher().on(MessageCreateEvent.class)
                .filter(event -> event.getMessage().getChannel().block().getType().equals(Channel.Type.GUILD_TEXT))
                .subscribe(event -> Utils.incrementName(event.getMember().get()));

        client.getEventDispatcher().on(ReactionAddEvent.class)
                .subscribe(Utils::handleClose);

        client.getEventDispatcher().on(MemberJoinEvent.class)
                .subscribe(memberJoinEvent -> {
                    memberJoinEvent.getMember().addRole(Snowflake.of(ConfigManager.config.get("guild").getAsJsonObject().get("shalomniker").getAsLong())).block();
                });

        client.login().block();
    }
}
