package ga.jakeobrien.shalombot3.responses;

import discord4j.core.event.domain.message.MessageCreateEvent;

import java.util.ArrayList;

public class ResponseManager {
    private static ArrayList<Response> responses = new ArrayList<>();

    public static void register(Response r) {
        responses.add(r);
    }

    public static void onMessage(MessageCreateEvent event) {
        responses.forEach(response -> {
            if (response.responseCondition(event)) {
                event.getMessage().getChannel().block().createMessage(response.response(event)).block();
            }
        });
    }
}
