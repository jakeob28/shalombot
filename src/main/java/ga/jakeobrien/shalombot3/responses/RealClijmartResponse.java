package ga.jakeobrien.shalombot3.responses;

import discord4j.core.event.domain.message.MessageCreateEvent;

public class RealClijmartResponse implements Response {
    @Override
    public boolean responseCondition(MessageCreateEvent event) {
        return event.getMessage().getContent().orElse("").equalsIgnoreCase("itrc");
    }

    @Override
    public String response(MessageCreateEvent event) {
        event.getMessage().delete().block();
        return "I'm the real Clijmart";
    }
}
