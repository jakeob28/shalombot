package ga.jakeobrien.shalombot3.responses;

import discord4j.core.event.domain.message.MessageCreateEvent;

public class ShouldWeKillfarmResponse implements Response {
    @Override
    public boolean responseCondition(MessageCreateEvent event) {
        String content = event.getMessage().getContent().orElse("");
        return content.toLowerCase().equals("should we killfarm") || content.toLowerCase().startsWith("should we killfarm ") || content.toLowerCase().endsWith(" should we killfarm") || content.toLowerCase().contains(" should we killfarm ");
    }

    @Override
    public String response(MessageCreateEvent event) {
        return "aww";
    }
}
