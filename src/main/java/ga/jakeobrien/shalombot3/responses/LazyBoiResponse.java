package ga.jakeobrien.shalombot3.responses;

import discord4j.core.event.domain.message.MessageCreateEvent;

public class LazyBoiResponse implements Response {
    @Override
    public boolean responseCondition(MessageCreateEvent event) {
        String content = event.getMessage().getContent().orElse("");
        return content.toLowerCase().equals("cba") || content.toLowerCase().startsWith("cba ") || content.toLowerCase().endsWith(" cba") || content.toLowerCase().contains(" cba ");
    }

    @Override
    public String response(MessageCreateEvent event) {
        return "lazy boi";
    }
}
