package ga.jakeobrien.shalombot3.responses;

import discord4j.core.event.domain.message.MessageCreateEvent;

public interface Response {
    boolean responseCondition(MessageCreateEvent event);
    String response(MessageCreateEvent event);
}
