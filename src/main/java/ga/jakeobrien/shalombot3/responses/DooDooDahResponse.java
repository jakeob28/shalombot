package ga.jakeobrien.shalombot3.responses;

import discord4j.core.event.domain.message.MessageCreateEvent;

public class DooDooDahResponse implements Response {
    @Override
    public boolean responseCondition(MessageCreateEvent event) {
        String content = event.getMessage().getContent().orElse("");
        return content.toLowerCase().equals("layta") || content.toLowerCase().startsWith("layta ") || content.toLowerCase().endsWith(" layta") || content.toLowerCase().contains(" layta ");
    }

    @Override
    public String response(MessageCreateEvent event) {
        return "doo doo dah";
    }
}
