package ga.jakeobrien.shalombot3.responses;

import discord4j.core.event.domain.message.MessageCreateEvent;

public class PurimResponse implements Response {
    @Override
    public boolean responseCondition(MessageCreateEvent event) {
        return event.getMessage().getContent().orElse("").equalsIgnoreCase("purim");
    }

    @Override
    public String response(MessageCreateEvent event) {
        event.getMessage().delete().block();
        return "Purim is one of the most joyous and fun holidays on the Jewish calendar.";
    }
}
