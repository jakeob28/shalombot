package ga.jakeobrien.shalombot3.responses;

import discord4j.core.event.domain.message.MessageCreateEvent;

public class WaterPumpResponse implements Response {
    @Override
    public boolean responseCondition(MessageCreateEvent event) {
        String content = event.getMessage().getContent().orElse("");
        return content.toLowerCase().equals("well") || content.toLowerCase().startsWith("well ") || content.toLowerCase().endsWith(" well") || content.toLowerCase().contains(" well ");
    }

    @Override
    public String response(MessageCreateEvent event) {
        return "water pump";
    }
}
