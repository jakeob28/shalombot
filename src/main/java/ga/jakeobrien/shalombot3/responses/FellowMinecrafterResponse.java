package ga.jakeobrien.shalombot3.responses;

import discord4j.core.event.domain.message.MessageCreateEvent;

public class FellowMinecrafterResponse implements Response {
    @Override
    public boolean responseCondition(MessageCreateEvent event) {
        return event.getMessage().getContent().orElse("").equalsIgnoreCase("htfm");
    }

    @Override
    public String response(MessageCreateEvent event) {
        event.getMessage().delete().block();
        return "Hey there fellow minecrafter.";
    }
}
