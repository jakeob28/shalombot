package ga.jakeobrien.shalombot3.commands;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Role;
import discord4j.core.object.util.Snowflake;

import java.util.ArrayList;
import java.util.List;

public class CommandManager {
    private static ArrayList<Command> commands = new ArrayList<>();

    public static void register(Command c) {
        commands.add(c);
    }

    public static void onMessage(MessageCreateEvent event) {
        List<Role> roles;
        if (event.getMember().isPresent()) {
            roles = event.getMember().get().getRoles().collectList().block();
        } else {
            roles = new ArrayList<>();
        }

        commands.forEach(command -> {
            String[] args = event.getMessage().getContent().orElse("").split(" ");

            if (!command.getName().equalsIgnoreCase(args[0])) {
                return;
            }

            if (command.requiredRoles().length > 0) {
                for (Snowflake roleID : command.requiredRoles()) {
                    if (!roles.contains(event.getGuild().block().getRoleById(roleID).block())) {
                        return;
                    }
                }
            }

            command.execute(event, args);
        });
    }
}
