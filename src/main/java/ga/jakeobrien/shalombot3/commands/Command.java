package ga.jakeobrien.shalombot3.commands;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.util.Snowflake;

public interface Command {
    void execute(MessageCreateEvent event, String [] args);

    String getName();

    Snowflake[] requiredRoles();
}
