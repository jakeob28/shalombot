package ga.jakeobrien.shalombot3.commands;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.PermissionOverwrite;
import discord4j.core.object.entity.TextChannel;
import discord4j.core.object.util.Permission;
import discord4j.core.object.util.PermissionSet;
import discord4j.core.object.util.Snowflake;
import ga.jakeobrien.shalombot3.Main;
import ga.jakeobrien.shalombot3.utils.ConfigManager;
import ga.jakeobrien.shalombot3.utils.Utils;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class TicketCommand implements Command {
    @Override
    public void execute(MessageCreateEvent event, String[] args) {
        if (!event.getMember().isPresent()) {
            return;
        }

        if (event.getMember().get().getRoleIds().contains(Snowflake.of(ConfigManager.config.get("guild").getAsJsonObject().get("ticket-banned").getAsLong()))) {
            event.getMessage().getChannel().block().createMessage("You are ticket banned!").block();
            return;
        }

        Set<PermissionOverwrite> perms = new HashSet<>();
        perms.add(PermissionOverwrite.forRole(
                Main.client.getGuildById(Snowflake.of(ConfigManager.config.get("guild").getAsJsonObject().get("id").getAsLong())).block().getEveryoneRole().block().getId(),
                PermissionSet.none(),
                PermissionSet.of(Permission.VIEW_CHANNEL))); //EVERYONE
        perms.add(PermissionOverwrite.forRole(
                Snowflake.of(ConfigManager.config.get("guild").getAsJsonObject().get("support-team").getAsLong()),
                PermissionSet.of(Permission.SEND_MESSAGES, Permission.VIEW_CHANNEL),
                PermissionSet.none())); //SUPPORT TEAM
        perms.add(PermissionOverwrite.forMember(
                event.getMember().get().getId(),
                PermissionSet.of(Permission.SEND_MESSAGES, Permission.VIEW_CHANNEL),
                PermissionSet.none())); //TICKET CREATOR

        TextChannel channel = Main.client.getGuildById(Snowflake.of(ConfigManager.config.get("guild").getAsJsonObject().get("id").getAsLong())).block().createTextChannel(textChannelCreateSpec -> {
            textChannelCreateSpec.setName("ticket-" + Utils.nextTicketNumber());
            textChannelCreateSpec.setParentId(Snowflake.of(ConfigManager.config.get("guild").getAsJsonObject().get("ticket-category").getAsLong()));
            textChannelCreateSpec.setTopic(event.getMember().get().getNickname().orElse("") + "'s ticket");
            textChannelCreateSpec.setPermissionOverwrites(perms);
        }).block();

        channel.createEmbed(embedCreateSpec -> {
            embedCreateSpec.setTitle("New ticket");
            embedCreateSpec.setColor(new Color(76, 175, 80));
            embedCreateSpec.setThumbnail("https://cdn2.iconfinder.com/data/icons/flaturici-set-4/512/ticket-512.png");
            embedCreateSpec.addField("Ticket Author", event.getMember().get().getMention(), true);
            embedCreateSpec.addField("Ticket ID", channel.getName(), true);
        }).block();

        channel.createMessage("Before asking for support on your newly created ticket, please read our simple terms of service.\n" +
                "•    First of all, keep in mind that tickets may not be private, and may be used for Shalom Support Team ™ training, and also examples for our members.\n" +
                "•    Second, you should be careful about holding information back. If you cannot provide a full conversation, we cannot provide the best answer for you.\n" +
                "•    Please note that all tickets may cost you up to several thousands of dollars.\n" +
                "•    Don’t forget that the Shalom Support Team ™ is completely serious and all of our answers take careful critiquing, and contemplation. \n" +
                "•    Releasing information about the Shalom Support Team ™ and fellow members of the Shalom Support Group ™, is punishable by full force explosions.\n" +
                "•    The Shalom Support Team ™ has full control over your ability to create tickets. Our team takes the upmost importance to fulfill your tickets accurately, so please do not fool around with our ticketing service. If you do, we will ban your ticket making ability.\n" +
                "•    Keep in mind the Shalom Support Team ™ may be offline and not able to answer your ticket at any possible time.\n" +
                "•    If your ticket is taking time to be processed please be patient. If you have waited over 24 hours then you my contact a Shalom Supporter ™ outside of your ticket.\n" +
                "•    All tickets are logged in the event that there is a complaint against a member of support staff or a member. If you would like a copy of your ticket's log, let us know.\n").block();

        event.getMessage().getChannel().block().createMessage("Created ticket at " + channel.getMention()).block();
    }

    @Override
    public String getName() {
        return "!ticket";
    }

    @Override
    public Snowflake[] requiredRoles() {
        return new Snowflake[0];
    }
}
