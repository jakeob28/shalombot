package ga.jakeobrien.shalombot3.commands;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.util.Snowflake;
import ga.jakeobrien.shalombot3.utils.ConfigManager;
import ga.jakeobrien.shalombot3.utils.Utils;

public class CloseCommand implements Command {
    @Override
    public void execute(MessageCreateEvent event, String[] args) {
        Utils.sendCloser(event.getMessage().getChannel().block());
    }

    @Override
    public String getName() {
        return "!close";
    }

    @Override
    public Snowflake[] requiredRoles() {
        return new Snowflake[] {Snowflake.of(ConfigManager.config.get("guild").getAsJsonObject().get("support-team").getAsLong())};
    }
}
