package ga.jakeobrien.shalombot3.commands;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.PermissionOverwrite;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.TextChannel;
import discord4j.core.object.util.Permission;
import discord4j.core.object.util.PermissionSet;
import discord4j.core.object.util.Snowflake;
import ga.jakeobrien.shalombot3.Main;
import ga.jakeobrien.shalombot3.utils.ConfigManager;

import java.util.List;

public class AddCommand implements Command {
    @Override
    public void execute(MessageCreateEvent event, String[] args) {
        if (!((TextChannel) event.getMessage().getChannel().block()).getName().startsWith("ticket-")) {
            event.getMessage().getChannel().block().createMessage("no").block();
            return;
        }

        List<Member> members = Main.client.getGuildById(Snowflake.of(ConfigManager.config.get("guild").getAsJsonObject().get("id").getAsLong())).block().getMembers().collectList().block();
        for (Member m : members) {
            if (m.getDisplayName().equalsIgnoreCase(args[1]) || (m.getNickname().isPresent() && m.getNickname().get().equalsIgnoreCase(args[1]))) {
                ((TextChannel) event.getMessage().getChannel().block()).addMemberOverwrite(
                        m.getId(),
                        PermissionOverwrite.forMember(m.getId(), PermissionSet.of(Permission.SEND_MESSAGES, Permission.VIEW_CHANNEL), PermissionSet.none())
                ).block();
            }
        }
    }

    @Override
    public String getName() {
        return "!add";
    }

    @Override
    public Snowflake[] requiredRoles() {
        return new Snowflake[] {Snowflake.of(ConfigManager.config.get("guild").getAsJsonObject().get("support-team").getAsLong())};
    }
}
